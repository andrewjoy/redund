TARGET = bin/redund.exe

SRCFILES = main.c
INCFILES = error.h

all: directories $(TARGET)

clean:
	rm -f $(TARGET)

rebuild: clean all

directories: bin

bin:
	mkdir bin/

$(TARGET) : $(SRCFILES) $(INCFILES)
	gcc -o $@ $(SRCFILES)