#ifndef __REDUND_ERROR__
#define __REDUND_ERROR__

struct error
{
    int code;
    char const* message;
};

static struct error const ERROR_ARGUMENT_MISSING = { -1, "too few arguments" };
static struct error const ERROR_ARGUMENT_COMMAND_OVERFLOW = {-2, "command argument overflow"};
static struct error const ERROR_INPUT_READ = { -100, "input read failed" };
static struct error const ERROR_INPUT_OVERFLOW = { -101, "input exceeded buffer length" };
static struct error const ERROR_COMMAND_FAIL = { -201, "command execution failed" };

#define FAIL(error) printf("redund error (%d): %s", error.code, error.message); return error.code;

#endif