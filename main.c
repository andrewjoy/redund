#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#include "error.h"

#define CONSOLE_COLOR_MAIN "\033[0;36;40m"
#define CONSOLE_COLOR_RESET "\x1b[0m"

int const BUFFER_LENGTH = 256;

int main(int argc, char const* const* argv)
{
    if (argc <= 1)
    {
        FAIL(ERROR_ARGUMENT_MISSING)
    }
    
    char command[BUFFER_LENGTH];
    
    // allows input overflow detection (comparison against '\0' for full read)
    command[BUFFER_LENGTH - 1] = '\n';
    
    char const* const commandBin = argv[1];
    size_t const commandBinLength = strlen(commandBin);
    
    if (commandBinLength >= BUFFER_LENGTH)
    {
        FAIL(ERROR_ARGUMENT_COMMAND_OVERFLOW)
    }
    
    strcpy(command, commandBin);
    command[commandBinLength] = ' ';
    
    char* const commandArgs = command + commandBinLength + 1;
    size_t const commandArgsLengthMax = BUFFER_LENGTH - commandBinLength - 1;
    
    do 
    {
        printf(CONSOLE_COLOR_MAIN "redund (%s):" CONSOLE_COLOR_RESET " ", commandBin);
        
        if (fgets(commandArgs, commandArgsLengthMax, stdin) == 0)
        {
            FAIL(ERROR_INPUT_READ)
        }
        
        if (command[BUFFER_LENGTH - 1] == '\0')
        {
            FAIL(ERROR_INPUT_OVERFLOW)
        }
        
        if (system(command) != 0)
        {
            FAIL(ERROR_COMMAND_FAIL)
        }
    }
    while (true);
    
    return 0;
}