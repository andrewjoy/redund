# redund
`redund` is a command line tool that reduces redundancy when issuing commands to the same binary in sequence.

~~~ sh
$ git status
$ git add .
$ git commit
$ git status
$ git log
~~~

becomes

~~~
$ redund git
$ status
$ add .
$ commit
$ status
$ log
~~~

Most binaries work with `redund`, including `vim` and `rm`. A notable exception is `cd`, which isn't compatible yet.
